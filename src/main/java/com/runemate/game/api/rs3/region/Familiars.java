package com.runemate.game.api.rs3.region;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.rs3.entities.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import java.util.*;
import java.util.function.*;

@Deprecated
public final class Familiars {
    private Familiars() {
    }

    public static List<SummonedFamiliar> getLoaded(final Predicate<SummonedFamiliar> filter) {
        return Collections.emptyList();
    }

    public static List<SummonedFamiliar> getLoaded(final Summoning.Familiar... types) {
        return Collections.emptyList();
    }

    public static List<SummonedFamiliar> getLoaded() {
        return Collections.emptyList();
    }

    public static List<SummonedFamiliar> getLoadedWithin(
        final Area area,
        final Predicate<SummonedFamiliar> filter
    ) {
        return Collections.emptyList();
    }

    public static List<SummonedFamiliar> getLoadedWithin(
        final Area area,
        final Summoning.Familiar... types
    ) {
        return Collections.emptyList();
    }

    public static List<SummonedFamiliar> getLoadedWithin(final Area area) {
        return Collections.emptyList();
    }

    public static Predicate<SummonedFamiliar> getTypePredicate(final Summoning.Familiar... types) {
        return ignored -> false;
    }

    public static Predicate<SummonedFamiliar> getAreaPredicate(final Area... areas) {
        return ignored -> false;
    }
}
