package com.runemate.game.api.rs3.local;

import org.jetbrains.annotations.*;

@Deprecated
public enum CombatMode {
    FULL_MANUAL,
    LEGACY,
    REVOLUTION;

    CombatMode() {
    }

    @Nullable
    public static CombatMode getCurrent() {
        return null;
    }

    public boolean isCurrent() {
        return false;
    }

    public boolean select() {
        return false;
    }

    @Override
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase().replace("_", " ");
    }
}
